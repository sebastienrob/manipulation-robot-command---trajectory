function [xyz_desire,derive_xyz_desire] = EvalTraj_operationnel(CoeffTraj,t)
global dt;
xyz_desire = CoeffTraj(:,4) + CoeffTraj(:,3)*t + CoeffTraj(:,2)*t^2 + CoeffTraj(:,1)*t^3; %1L 7C
%%application num�rique ok jusqu'ici

if t==0
    derive_xyz_desire = zeros(3,1);
else
    xyz_desire_precedent = CoeffTraj(:,4) + CoeffTraj(:,3)*(t-dt) + CoeffTraj(:,2)*(t-dt)^2 + CoeffTraj(:,1)*(t-dt)^3;
    derive_xyz_desire = (xyz_desire - xyz_desire_precedent)/(t-(t-dt));
end

end