function [J] = Jacobienne(robot)

%calcul des zi dans 0
for i = 1:7
    
zi = robot.link(i).T_0(1:3,3); %calcul des zi dans le rep�re 0
li = robot.link(7).T_0(1:3,4) - robot.link(i).T_0(1:3,4); %calcul des li dans le rep�re 0
ziXli = cross(zi,li); %le produit vectorielle

J_i = [ziXli(1,1) ; ziXli(2,1) ; ziXli(3,1) ; zi(1,1) ; zi(2,1) ; zi(3,1)]; %jacobienne pour chaque articulation
robot.link(i).J = J_i;
end

% matrice jacobienne enti�re 
J = [robot.link(1).J robot.link(2).J robot.link(3).J robot.link(4).J robot.link(5).J robot.link(6).J robot.link(7).J];

end
