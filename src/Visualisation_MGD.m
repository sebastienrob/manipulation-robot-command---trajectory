function [] = Visualisation_MGD(robot)
%-->ouvrir figure avant de faire appel � la fonction
clf;
for i=1:7     
    
    %pose des axes de chaques rep�res
    robot.link(i).pose_origin = robot.link(i).T_0 * [0 ; 0 ; 0 ; 1]; %pour le point d'origine de xi
    robot.link(i).pose_x = robot.link(i).T_0 * [0.1 ; 0 ; 0 ; 1]; %pose vecteur xi
    robot.link(i).pose_y = robot.link(i).T_0 * [0 ; 0.1 ; 0 ; 1]; %pose vecteur yi
    robot.link(i).pose_z = robot.link(i).T_0 * [0 ; 0 ; 0.1 ; 1]; %pose vecteur zi
     
    %tracer la jointure des origines des rep�res
    if i == 1 
    line([0 robot.link(i).pose_origin(1)],[0 robot.link(i).pose_origin(2)],[0 robot.link(i).pose_origin(3)], 'LineWidth',2,'Color','green','LineStyle','--'); %segments
    else
    line([robot.link(i-1).pose_origin(1) robot.link(i).pose_origin(1)],[robot.link(i-1).pose_origin(2) robot.link(i).pose_origin(2)],[robot.link(i-1).pose_origin(3) robot.link(i).pose_origin(3)], 'LineWidth',2,'Color','green','LineStyle','--'); %segments
    end

    %tracer les axes des rep�res   
    line([robot.link(i).pose_origin(1) robot.link(i).pose_x(1)],[robot.link(i).pose_origin(2) robot.link(i).pose_x(2)],[robot.link(i).pose_origin(3) robot.link(i).pose_x(3)], 'LineWidth',2,'Color','blue');
    line([robot.link(i).pose_origin(1) robot.link(i).pose_y(1)],[robot.link(i).pose_origin(2) robot.link(i).pose_y(2)],[robot.link(i).pose_origin(3) robot.link(i).pose_y(3)], 'LineWidth',2,'Color','red');
    line([robot.link(i).pose_origin(1) robot.link(i).pose_z(1)],[robot.link(i).pose_origin(2) robot.link(i).pose_z(2)],[robot.link(i).pose_origin(3) robot.link(i).pose_z(3)], 'LineWidth',2,'Color','black');
    
end
%axis([-0.45 0.1 -0.8 0.2 0 1.2]) %pour articulaire 
axis([-1 0.5 -1 0.5 -0.2 1.2]) %pour arc de cercle trajectoire 
%axis([-1 1 -1 1 1 1.2]) 
grid on
title('Visualisation Espace Op�rationnel');
legend('segments','axe x','axe y','axe z')
xlabel('axe x')
ylabel('axe y')
zlabel('axe z')
view([90,90,90])
drawnow;

