function [CoeffTraj] = GenTraj(Tf)
global x0 xf
a = -2*(xf(1:7) - x0(1:7))/(Tf)^3;
b = 3 * (xf(1:7) - x0(1:7))/(Tf)^2;
c = zeros(7,1);
d = x0(1:7);
CoeffTraj = [a b c d]; %1L et 7C
end