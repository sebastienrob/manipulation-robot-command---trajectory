function dx = ModDyn(~,x,u)
global aj Kj
dq = x(8:14); 
dx1 = dq; %vitesses
dx2 = (Kj/aj)*u - (1/aj)*dq; %accelerations
dx = [dx1 ; dx2]; % 2L et 7C // [vitesses ; accelerations]
end