function [q_desire,derive_q_desire] = EvalTraj(CoeffTraj,t)
global dt;
q_desire = CoeffTraj(:,4) + CoeffTraj(:,3)*t + CoeffTraj(:,2)*t^2 + CoeffTraj(:,1)*t^3; %1L 7C
%%application num�rique ok jusqu'ici

if t==0
    derive_q_desire = zeros(7,1);
else
    q_desire_precedent = CoeffTraj(:,4) + CoeffTraj(:,3)*(t-dt) + CoeffTraj(:,2)*(t-dt)^2 + CoeffTraj(:,1)*(t-dt)^3;
    derive_q_desire = (q_desire - q_desire_precedent)/(t-(t-dt));
end

end
