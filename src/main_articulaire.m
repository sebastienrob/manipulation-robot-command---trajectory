clear all
close all
clc

%% INFORMATION
%%VREP --> kuka LBR4
%Kd et Kp � determiner par la fonction de transfert

%% PARAMETRES
global aj Kj x0 xf dt; 
aj = 0.1;
Kj = 10;
w0 = 10;
Kp = w0^2/Kj;
Kd = (2*w0-1)/Kj;

x0 = zeros(14,1);  %position et vitesse initial
%attention q limit� � [-2.9671;2.9671]radian et d_q � 3.1415rad/s pour A4 et 1.9635rad/s pour les autres 
xf = [pi/2 ; pi/3 ; pi/4 ; pi/6 ; pi/4 ; (2*pi)/3 ; pi/5 ; 0 ; 0 ; 0 ; 0 ; 0 ; 0 ; 0]; %position et vitesse final d�sir� THETA en rad/s
Kp = 10;
Kd = 1.9;
tf = 10; %dur�e du process (generation de trajectoire)

%% GENERATION PARAMETRES TRAJECTOIRE
%N.B : ici dans l'espace articulaires mais on peut la faire aussi dans
%l'espace op�rationnel
[CoeffTraj] = GenTraj(tf); %%matrice des coeff pour trajectoire

%% BOUCLE ASSERVISSEMENT CONTROLE
q = x0(1:7);
dq = x0(8:14);
dt = 10^-3;
taille_echantillon = 10/dt;
q_desire_echantillonner = zeros(taille_echantillon,7);
derive_q_desire_echantillonner = zeros(taille_echantillon,7);
q_reel_echantillonner = zeros(taille_echantillon,7);
derive_q_reel_echantillonner = zeros(taille_echantillon,7);
temps = zeros(taille_echantillon,1);
i=1;
for t = 0:dt:tf

%% ESPACE ARTICULAIRE
%calcul de la nouvelle position et vitesse d�sir� par le bloc de generation de trajectoire    
[q_desire,derive_q_desire] = EvalTraj(CoeffTraj,t);

%sauvegarde des qDesire
q_desire_echantillonner(i,:) = q_desire(1:7,:);
derive_q_desire_echantillonner(i,:) = derive_q_desire(1:7,:);
temps(i,1) = t;

%calcul de la nouvelle commande(couple) par le bloc correcteur Kp
u = Kp*(q_desire - q) + Kd*(derive_q_desire - dq); %u = tau = vecteur de couple

%les pr�c�dents q et dq
x0 = [q ; dq];

%on int�gre le mod�le dynamique pour retrouver la position et vitesse
[T,X] = ode45(@(t,x) ModDyn(t,x,u), [0 10^-3], x0);

%les nouveaux q et dq
n = size(X,1);
xx = X(n,:)';
q = xx(1:7);
dq = xx(8:14);
x = [q ; dq];

%sauvegarde des qReel (en sortie du robot, modele dynamique)
q_reel_echantillonner(i,:) = x(1:7,1);
derive_q_reel_echantillonner(i,:) = x(8:14,1);
i=i+1;
%% ESPACE OPERATIONNEL
[robot] = MGD(q);
robot_save(i) = robot; %sauvegarde pour animation 3D = fonction du temps

end

%% VISUALISATION ESPACE ARTICULAIRE
figure;
plot(temps(:,1),q_desire_echantillonner(:,1),'b') %'b--'
hold on
plot(temps(:,1),q_desire_echantillonner(:,2),'g')
hold on
plot(temps(:,1),q_desire_echantillonner(:,3),'r')
hold on
plot(temps(:,1),q_desire_echantillonner(:,4),'c')
hold on
plot(temps(:,1),q_desire_echantillonner(:,5),'m')
hold on
plot(temps(:,1),q_desire_echantillonner(:,6),'y')
hold on
plot(temps(:,1),q_desire_echantillonner(:,7),'k')
hold on
plot(temps(:,1),q_reel_echantillonner(:,1),'b--') 
hold on
plot(temps(:,1),q_reel_echantillonner(:,2),'g--')
hold on
plot(temps(:,1),q_reel_echantillonner(:,3),'r--')
hold on
plot(temps(:,1),q_reel_echantillonner(:,4),'c--')
hold on
plot(temps(:,1),q_reel_echantillonner(:,5),'m--')
hold on
plot(temps(:,1),q_reel_echantillonner(:,6),'y--')
hold on
plot(temps(:,1),q_reel_echantillonner(:,7),'k--')
legend('axe 1D','axe 2D','axe 3D','axe 4D','axe 5D','axe 6D','axe 7D','axe 1R','axe 2R','axe 3R','axe 4R','axe 5R','axe 6R','axe 7R')
title('Position D�sir� et R�el')
xlabel('temps en seconde')
ylabel('angle en radian')

figure;
plot(temps(:,1),derive_q_desire_echantillonner(:,1),'b')
hold on
plot(temps(:,1),derive_q_desire_echantillonner(:,2),'g')
hold on
plot(temps(:,1),derive_q_desire_echantillonner(:,3),'r')
hold on
plot(temps(:,1),derive_q_desire_echantillonner(:,4),'c')
hold on
plot(temps(:,1),derive_q_desire_echantillonner(:,5),'m')
hold on
plot(temps(:,1),derive_q_desire_echantillonner(:,6),'y')
hold on
plot(temps(:,1),derive_q_desire_echantillonner(:,7),'k')
hold on
plot(temps(:,1),derive_q_reel_echantillonner(:,1),'b--')
hold on
plot(temps(:,1),derive_q_reel_echantillonner(:,2),'g--')
hold on
plot(temps(:,1),derive_q_reel_echantillonner(:,3),'r--')
hold on
plot(temps(:,1),derive_q_reel_echantillonner(:,4),'c--')
hold on
plot(temps(:,1),derive_q_reel_echantillonner(:,5),'m--')
hold on
plot(temps(:,1),derive_q_reel_echantillonner(:,6),'y--')
hold on
plot(temps(:,1),derive_q_reel_echantillonner(:,7),'k--')
legend('axe 1D','axe 2D','axe 3D','axe 4D','axe 5D','axe 6D','axe 7D','axe 1R','axe 2R','axe 3R','axe 4R','axe 5R','axe 6R','axe 7R')
title('Vitesse D�sir� et R�el')
xlabel('temps en seconde')
ylabel('vitesse en rad/s')

%% ANIMATION 3D DE L'ESPACE OPERATIONNEL MGD
filename = 'R_L_CdM_yes.gif';
figure;
grid on
axis([-0.45 0.1 -0.8 0.2 0 1.2]) 
hold on;
%tracer les axes du rep�re R0 et point d'origine
line([0 0.1],[0 0],[0 0], 'LineWidth',2,'Color','blue'); %x
line([0 0],[0 0.1],[0 0], 'LineWidth',2,'Color','red'); %y
line([0 0],[0 0],[0 0.1], 'LineWidth',2,'Color','black'); %z
plot3(0,0,0,'*','markersize',10,'LineWidth',2.5,'Color','black');

% ma fonction d'affichage BOUCLE ICI
for j = 2:250:10002
Visualisation_MGD(robot_save(j))

% Creation du gif
       M(j) = getframe(gcf);
       im = frame2im(M(j)); 
      [imind,cm] = rgb2ind(im,256); 
      % Write to the GIF File 
      if j == 2
          imwrite(imind,cm,filename,'gif', 'Loopcount',inf); 
      else 
          imwrite(imind,cm,filename,'gif','WriteMode','append'); 
      end 
end