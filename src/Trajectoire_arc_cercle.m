function [xyz_desire,derive_xyz_desire] = Trajectoire_arc_cercle()

C = [0 ; 0 ; 1];
R = 0.3;
phi = 0;
for j = 1:20 %nombre de positions interm�diaires
x(j) = C(1) + R * cos(phi);
z(j) = C(2) + R * sin(phi);
phi = phi + pi/30;
end

%graph de la trajectoire
Pi = [x ; z];
plot(Pi(1,:),Pi(2,:));
title('Trajectoire en arc de cercle d�sir�')
xlabel('x en m')
ylabel('z en m')

%cr�ations des splines avec vitesses nulles au d�but et � la fin
t = [0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19];
tq = 0:10^-3:10;
slope0 = 0;
slopeF = 0;
x = Pi(1,:)';
y = Pi(2,:)';
xq = spline(t,[slope0; x; slopeF],tq);
yq = spline(t,[slope0; y; slopeF],tq);

% plot spline in t-x, t-y and x-y space
figure;
plot(t,x,'o',tq,xq,':.');
axis([-0.5 5.5 -0.5 1.5]);
title('x vs. t');
figure;
plot(t,y,'o',tq,yq,':.');
axis([-0.5 5.5 -0.5 2.5]);
title('y vs. t');
figure;
plot(x,y,'o',xq,yq,':.');
axis([-0.5 1.5 -0.5 2.5]);
title('y vs. x');

trajectoire(1,:) = xq;
trajectoire(2,:) = yq;

%obtention des vitesses d�sir�
for i = 1:10001
    xyz_desire(i,:) = [trajectoire(1,i) ; 0.01 ; trajectoire(2,i)];
    if i == 1
        derive_x_desire(1,i) = 0;
        derive_z_desire(1,i) = 0;
    else
        derive_x_desire(1,i) = (trajectoire(1,i) - trajectoire(1,i-1))/(10^-3);
        derive_z_desire(1,i) = (trajectoire(2,i) - trajectoire(2,i-1))/(10^-3);
    end

derive_xyz_desire(i,:) = [derive_x_desire(1,i) ; 0 ; derive_z_desire(1,i)];
end

end