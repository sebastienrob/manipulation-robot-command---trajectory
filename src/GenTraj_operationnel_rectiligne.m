function [CoeffTraj] = GenTraj_operationnel_rectiligne(Tf)
global x0 xf
c = (xf(1:3) - x0(1:3))/Tf;
d = x0(1:3);
CoeffTraj = [c d]; %1L et 3C
end