clear all
close all
clc

%% INFORMATION
%%VREP --> kuka LBR4
%Kd et Kp � determiner par la fonction de transfert

%% PARAMETRES
global aj Kj x0 xf dt; 
aj = 0.1;
Kj = 10;
w0 = 10;
% Kp = ((w0^2)*aj)/Kj;
% Kd = ((2*Kp)/w0) - (1/Kj);

%q init 
x1 = zeros(14,1);
q = [0.01;0.01;0.01;0.01;0.01;0.01;0.01];
dq = x1(8:14);
[robot] = MGD(q);
robot.link(7).pose_origin = robot.link(7).T_0 * [0 ; 0 ; 0 ; 1]; %pour la pose du dernier rep�re 
xyz = robot.link(7).pose_origin(1:3); 
x0 = [xyz(1) ; xyz(2) ; xyz(3) ; 0 ; 0 ; 0]; %position et vitesse initial

%attention q limit� � [-2.9671;2.9671]radian et d_q � 3.1415rad/s pour A4 et 1.9635rad/s pour les autres 
xf = [0.3 ; 0.01 ; 0 ; 0 ; 0 ; 0]; %position et vitesse final d�sir�
Kp = 10;
Kd = 1.9;
tf = 10; %dur�e du process (generation de trajectoire)

%% GENERATION PARAMETRES TRAJECTOIRE
%N.B : ici dans l'espace articulaires mais on peut la faire aussi dans
%l'espace op�rationnel
[CoeffTraj] = GenTraj_operationnel(tf); %%matrice des coeffs pour trajectoire

%% BOUCLE ASSERVISSEMENT CONTROLE
xyz = x0(1:3);
d_xyz = x0(4:6);
dt = 10^-3;
taille_echantillon = 10/dt;
q_consigne_echantillonner = zeros(taille_echantillon,7);
derive_q_consigne_echantillonner = zeros(taille_echantillon,7);

q_reel_echantillonner = zeros(taille_echantillon,7);
derive_q_reel_echantillonner = zeros(taille_echantillon,7);
temps = zeros(taille_echantillon,1);
i=1;
K=1;
k=1;
q_consigne = [0.01;0.01;0.01;0.01;0.01;0.01;0.01];
for t = 0:dt:tf
    
%% ESPACE OPERATIONNEL

%calcul de la nouvelle position et vitesse d�sir� par le bloc de generation de trajectoire    
[xyz_desire,derive_xyz_desire] = EvalTraj_operationnel(CoeffTraj,t);

%Calcul de la jacobienne de R7 dans R0
[J] = Jacobienne(robot);
J_xyz = J(1:3,1:7); %jacobienne pour vitesse en x,y,z
Pinverse_J = pinv(J_xyz);

%calcul des vitesses articulaires--> BLOC
q_derive = Pinverse_J * (derive_xyz_desire + K * (xyz_desire - xyz));

%calcul de la consigne en position (donc les q d�sir�)
q_consigne_precedent = q_consigne;
q_consigne = q_consigne_precedent + q_derive * dt;
q_consigne_derive = (q_consigne - q_consigne_precedent)/(t-(t-dt));

%Limitation de la consigne
% for v=1:7
%    if (v == 5) && (abs(q_consigne_derive(v)) > 0.1800)
%        q_consigne_derive(v) = 0.1800;
%    elseif (v ~= 5) && (abs(q_consigne_derive(v)) > 0.1125)
%        q_consigne_derive(v) = 0.1125;
%    end
%    
%    if ((v == 1) || (v == 3) || (v == 5) || (v == 7)) && (q_consigne(v) > 2.9671 || q_consigne(v) < -2.9671)
%        if q_consigne(v) > 2.9671
%            q_consigne(v) = 2.9671;
%            if q_consigne(v) < -2.9671
%                q_consigne(v) = -2.9671;
%            end
%        end
%    end
%  
%       if ((v == 2) || (v == 4) || (v == 6)) && (q_consigne(v) > 2.0944 || q_consigne(v) < -2.0944)
%        if q_consigne(v) > 2.0944
%            q_consigne(v) = 2.0944;
%            if q_consigne(v) < -2.0944
%                q_consigne(v) = -2.0944;
%            end
%        end
%       end
% end

%% ESPACE ARTICULAIRE

%sauvegarde des qDesire
q_consigne_echantillonner(i,:) = q_consigne(1:7,:);
derive_q_consigne_echantillonner(i,:) = q_consigne_derive(1:7,:);
xyz_desire_echantillonner(i,1:3) = xyz_desire;
temps(i,1) = t;

%calcul de la nouvelle commande(couple) par le bloc correcteur Kp
u = Kp*(q_consigne - q) + Kd*(q_consigne_derive - dq); %u = tau = vecteur de couple

%les pr�c�dents q et dq
x0 = [q ; dq];

%on int�gre le mod�le dynamique pour retrouver la position et vitesse
[T,X] = ode45(@(t,x) ModDyn(t,x,u), [0 10^-3], x0);

%les nouveaux q et dq
n = size(X,1);
xx = X(n,:)';
q = xx(1:7);
dq = xx(8:14);
x = [q ; dq];

%sauvegarde des qReel (en sortie du robot, modele dynamique)
q_reel_echantillonner(i,:) = x(1:7,1);
derive_q_reel_echantillonner(i,:) = x(8:14,1);
i=i+1;

%MGD pour retrouver les x,y,z r�els qui seront renvoyer au bloc gen.Traj
[robot] = MGD(q);
robot_save(i) = robot; %sauvegarde pour animation 3D = fonction du temps
robot.link(7).pose_origin = robot.link(7).T_0 * [0 ; 0 ; 0 ; 1]; %pour la pose du dernier rep�re 
xyz = robot.link(7).pose_origin (1:3); % [xreal ;yreal ; zreal]

if i ~= 1
robot_save_xyz(i-1,1:3) = robot.link(7).pose_origin (1:3);
end
end

%% VISUALISATION ESPACE ARTICULAIRE
figure;
plot(temps(:,1),q_consigne_echantillonner(:,1),'b') %'b--'
hold on
plot(temps(:,1),q_consigne_echantillonner(:,2),'g')
hold on
plot(temps(:,1),q_consigne_echantillonner(:,3),'r')
hold on
plot(temps(:,1),q_consigne_echantillonner(:,4),'c')
hold on
plot(temps(:,1),q_consigne_echantillonner(:,5),'m')
hold on
plot(temps(:,1),q_consigne_echantillonner(:,6),'y')
hold on
plot(temps(:,1),q_consigne_echantillonner(:,7),'k')
hold on
plot(temps(:,1),q_reel_echantillonner(:,1),'b--') 
hold on
plot(temps(:,1),q_reel_echantillonner(:,2),'g--')
hold on
plot(temps(:,1),q_reel_echantillonner(:,3),'r--')
hold on
plot(temps(:,1),q_reel_echantillonner(:,4),'c--')
hold on
plot(temps(:,1),q_reel_echantillonner(:,5),'m--')
hold on
plot(temps(:,1),q_reel_echantillonner(:,6),'y--')
hold on
plot(temps(:,1),q_reel_echantillonner(:,7),'k--')
legend('axe 1D','axe 2D','axe 3D','axe 4D','axe 5D','axe 6D','axe 7D','axe 1R','axe 2R','axe 3R','axe 4R','axe 5R','axe 6R','axe 7R')
title('Position Consigne et R�el')
xlabel('temps en seconde')
ylabel('angle en radian')

figure;
plot(temps(:,1),derive_q_consigne_echantillonner(:,1),'b')
hold on
plot(temps(:,1),derive_q_consigne_echantillonner(:,2),'g')
hold on
plot(temps(:,1),derive_q_consigne_echantillonner(:,3),'r')
hold on
plot(temps(:,1),derive_q_consigne_echantillonner(:,4),'c')
hold on
plot(temps(:,1),derive_q_consigne_echantillonner(:,5),'m')
hold on
plot(temps(:,1),derive_q_consigne_echantillonner(:,6),'y')
hold on
plot(temps(:,1),derive_q_consigne_echantillonner(:,7),'k')
hold on
plot(temps(:,1),derive_q_reel_echantillonner(:,1),'b--')
hold on
plot(temps(:,1),derive_q_reel_echantillonner(:,2),'g--')
hold on
plot(temps(:,1),derive_q_reel_echantillonner(:,3),'r--')
hold on
plot(temps(:,1),derive_q_reel_echantillonner(:,4),'c--')
hold on
plot(temps(:,1),derive_q_reel_echantillonner(:,5),'m--')
hold on
plot(temps(:,1),derive_q_reel_echantillonner(:,6),'y--')
hold on
plot(temps(:,1),derive_q_reel_echantillonner(:,7),'k--')
legend('axe 1D','axe 2D','axe 3D','axe 4D','axe 5D','axe 6D','axe 7D','axe 1R','axe 2R','axe 3R','axe 4R','axe 5R','axe 6R','axe 7R')
title('Vitesse Consigne et R�el')
xlabel('temps en seconde')
ylabel('vitesse en rad/s')

%% VISUALISATION ESPACE OPERATIONNEL
figure;
plot(temps(:,1),robot_save_xyz(:,1),'b') %Position r�el
hold on
plot(temps(:,1),robot_save_xyz(:,2),'g')
hold on
plot(temps(:,1),robot_save_xyz(:,3),'r')
hold on
plot(temps(:,1),xyz_desire_echantillonner(:,1),'b--') %position d�sir�
hold on
plot(temps(:,1),xyz_desire_echantillonner(:,2),'g--')
hold on
plot(temps(:,1),xyz_desire_echantillonner(:,3),'r--')
legend('axe x r�el ','axe y r�el','axe z r�el','axe x d�sir�','axe y d�sir�','axe z d�sir�')
title('Position Consigne et R�el')
xlabel('temps en seconde')
ylabel('Position en m')

%% ANIMATION 3D DE L'ESPACE OPERATIONNEL MGD
filename = 'R_L_CdM_yes.gif';
figure;
grid on
axis([-0.45 0.1 -0.8 0.2 0 1.2]) 
hold on;
%tracer les axes du rep�re R0 et point d'origine
line([0 0.1],[0 0],[0 0], 'LineWidth',2,'Color','blue'); %x
line([0 0],[0 0.1],[0 0], 'LineWidth',2,'Color','red'); %y
line([0 0],[0 0],[0 0.1], 'LineWidth',2,'Color','black'); %z
plot3(0,0,0,'*','markersize',10,'LineWidth',2.5,'Color','black');

% ma fonction d'affichage BOUCLE ICI
for j = 2:250:10002
Visualisation_MGD(robot_save(j))

% Creation du gif
       M(j) = getframe(gcf);
       im = frame2im(M(j)); 
      [imind,cm] = rgb2ind(im,256); 
      % Write to the GIF File 
      if j == 2
          imwrite(imind,cm,filename,'gif', 'Loopcount',inf); 
      else 
          imwrite(imind,cm,filename,'gif','WriteMode','append'); 
      end 
end


figure;
plot3(xyz_desire_echantillonner(:,1),xyz_desire_echantillonner(:,2),xyz_desire_echantillonner(:,3),'g');
hold on;
plot3(robot_save_xyz(:,1),robot_save_xyz(:,2),robot_save_xyz(:,3),'r');
legend('trajectoire d�sir� ','trajectoire r�el');
title('Trajectoire apr�s commande cin�matique');
xlabel('axe x');
ylabel('axe y');
zlabel('axe z');
grid on;