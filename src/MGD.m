function [robot] = MGD(q)

%PARAMETRE DH
param_DH = zeros(7,4);
param_DH(:,1) = [ 0 ; pi/2 ; -pi/2 ; -pi/2; pi/2; pi/2 ; -pi/2]; %alpha (d(m) deja � 0)
param_DH(:,3) = q(1:7); %theta(t)-->les angles � l'instant t(q)
param_DH(:,4) = [0.3105 ; 0 ; 0.4 ; 0 ; 0.39 ; 0 ; 0.078]; %r(m)

for i = 1:7
%Ti_i-1 = Rot(x,param_DH(i,1))*Trans(x,param_DH(i,2))*Rot(z,param_DH(i,3))*Rot(z,param_DH(i,4))  
imoins1_T_i = [cos(param_DH(i,3)) -sin(param_DH(i,3)) 0 param_DH(i,2) ;
               cos(param_DH(i,1))*sin(param_DH(i,3)) cos(param_DH(i,1))*cos(param_DH(i,3)) -sin(param_DH(i,1)) -param_DH(i,4)*sin(param_DH(i,1)) ;
               sin(param_DH(i,1))*sin(param_DH(i,3)) sin(param_DH(i,1))*cos(param_DH(i,3)) cos(param_DH(i,1)) param_DH(i,4)*cos(param_DH(i,1)) ;
               0 0 0 1 ];
     
%T --> matrice de transformation homog�ne d�crivant situation d'un corp par rapport au pr�c�dent (R4-->R3 par exemple)           
robot.link(i).T = imoins1_T_i; 

%T_0 --> matrice de transformation d�crivant le corp i, rep�re i dans le rep�re 0
if i == 1
    robot.link(1).T_0 = robot.link(1).T;
else
    robot.link(i).T_0 = robot.link(i-1).T_0 * robot.link(i).T;
end

end

end
