function [CoeffTraj] = GenTraj_operationnel(Tf)
global x0 xf
a = -2*(xf(1:3) - x0(1:3))/(Tf)^3;
b = 3 * (xf(1:3) - x0(1:3))/(Tf)^2;
c = zeros(3,1);
d = x0(1:3);
CoeffTraj = [a b c d]; %1L et 3C
end