function [xyz_desire,derive_xyz_desire] = EvalTraj_operationnel_rectiligne(CoeffTraj,t)
global dt;
xyz_desire = CoeffTraj(:,2) + CoeffTraj(:,1)*t;
%%application num�rique ok jusqu'ici

if t==0
    derive_xyz_desire = zeros(3,1);
else
    xyz_desire_precedent = CoeffTraj(:,2) + CoeffTraj(:,1)*(t-dt);
    derive_xyz_desire = (xyz_desire - xyz_desire_precedent)/(t-(t-dt));
end

end